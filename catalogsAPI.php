<?php

/*	CatalogsAPI

	This simple web service script provides a RESTful API interface for an App
	that will provide:
	1) Catalogos DB access and response logic for queries;
	- Listar todos os diferentes fabricantes (= Publishers) i.e. marcas dos fabricantes
	2) All catalog Lines from one publisher response logic for Database catalogos;
	- Lista todas as Linhas de um fabricante
	3) All Categories from a Line of a Publisher response logic;
	- Lista todas as Categorias existentes na Linha yy do fabricante xx
	4) All sub-Categories from a Category's Line of one Publisher;
	- sub-Categorias existentes na Categoria zz da Linha yy do frabricante xx
	5) Error response logic.

	Input:

		$_GET['format'] = [ json | html | xml ]
		$_GET['method'] = []

	Output: A formatted HTTP response
	
       original: http://markroland.com/blog/restful-php-api/
       example:  http://blog.agupieware.com/2014/10/networking-in-swift-building-our.html        
       best:  http://coreymaynard.com/blog/creating-a-restful-api-with-php/        
*/

//  Step 1: Initialize variables and functions

	$dbhost = getenv("MYSQL_SERVICE_HOST");  // OPENSHIFT_MYSQL_DB_HOST
	$dbport = getenv("MYSQL_SERVICE_PORT");  // OPENSHIFT_MYSQL_DB_PORT
	$dbuser = getenv("MYSQL_USER");      // OPENSHIFT_MYSQL_DB_USERNAME
	$dbpwd =  getenv("MYSQL_PASSWORD");  // OPENSHIFT_MYSQL_DB_PASSWORD
	// $dbname = "catalogos";
	$dbname = getenv("MYSQL_DATABASE");

/**
 * Deliver HTTP Response
 * @param string $format The desired HTTP response content type: [json, html, xml]
 * @param string $api_response The desired HTTP response data
 * @return void
 **/
function deliver_response($format, $api_response){

	// Define HTTP responses
	$http_response_code = array(
		200 => 'OK',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		403 => 'Forbidden',
		404 => 'Not Found'
	);

	// Set HTTP Response
  // ?	header('HTTP/1.1 '.$api_response['status'].' '.$http_response_code[ $api_response['status'] ]);

	// ** Process different content types:
	
	if( strcasecmp($format,'json') == 0 ){
		// Set HTTP Response Content Type
		header('Content-Type: application/json; charset=utf-8');
	// Format data into a JSON response
		$json_response = json_encode($api_response, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
	// Deliver formatted data
		echo $json_response;

	}elseif( strcasecmp($format,'xml') == 0 ){

		// Set HTTP Response Content Type
		header('Content-Type: application/xml; charset=utf-8');
		// initializing or creating array //
		$data = $api_response['data']; 
		// Format data into an XML response //
		$xml = '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
		arrayToXml($data, $xml);       
		// Deliver formatted data //
		echo $xml;
		
	}else{
		// Set HTTP Response Content Type 
		header('Content-Type: text/html; charset=utf-8');
		// initializing ro creating array //
		$data = $api_response['data'];      
		$payload = '';
		$html = '';
		if (is_array($data)) {
			 arrayToHTML($data, $html);
			 $payload = $html;
		}
		else {
			$payload = $data;
		}      
		// Deliver formatted data //
		echo $payload;
	}
	// ** End script process	exit;
}

// ** Define whether an HTTPS connection is required
$HTTPS_required = FALSE;

// Define whether user authentication is required
$authentication_required = FALSE;

// Define API response codes and their related HTTP response
$api_response_code = array(
	0 => array('HTTP Response' => 400, 'Message' => 'Unknown Error'),
	1 => array('HTTP Response' => 200, 'Message' => 'Success'),
	2 => array('HTTP Response' => 403, 'Message' => 'HTTPS Required'),
	3 => array('HTTP Response' => 401, 'Message' => 'Authentication Required'),
	4 => array('HTTP Response' => 401, 'Message' => 'Authentication Failed'),
	5 => array('HTTP Response' => 404, 'Message' => 'Invalid Request'),
	6 => array('HTTP Response' => 400, 'Message' => 'Invalid Response Format')
);

// ** Set default HTTP response of of 'resource not found' - 'ok'
$response['code'] = 0;
$response['status'] = 404;
$response['data'] = NULL;

// --- Step 2: Authorization
// Optionally require connections to be made via HTTPS
if( $HTTPS_required && $_SERVER['HTTPS'] != 'on' ){
	$response['code'] = 2;
	$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
	$response['data'] = $api_response_code[ $response['code'] ]['Message'];

	// Return Response to browser. This will exit the script.
	deliver_response($_GET['format'], $response);
}

// Optionally require user authentication
if( $authentication_required ){
	if( empty($_POST['username']) || empty($_POST['password']) ){
		$response['code'] = 3;
		$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
		$response['data'] = $api_response_code[ $response['code'] ]['Message'];

		// Return Response to browser
		deliver_response($_GET['format'], $response);
	}
	// Return an error response if user fails authentication. This is a very simplistic example
	// that should be modified for security in a production environment
	elseif( $_POST['username'] != 'foo' && $_POST['password'] != 'bar' ){
		$response['code'] = 4;
		$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
		$response['data'] = $api_response_code[ $response['code'] ]['Message'];
	// Return Response to browser
		deliver_response($_GET['format'], $response);
	}
// https://www.functions-online.com/preg_match.html
}

// * Helper functions *

/**
 * Function returns XML string for input associative array.
 * @param Array $array Input associative array
 * @param String $wrap Wrapping tag
 * @param Boolean $upper To set tags in uppercase
 * Note: Function is an adaptation from -- http://www.redips.net/php/convert-array-to-xml/ 
 */
function arrayToXml($array, &$xml = '', $wrap='DATA', $upper=true) {
// wrap XML with $wrap TAG //
	if ($wrap != null) {
	    $xml .= "<$wrap>\n";
	}
	// main loop //
	foreach ($array as $key=>$value) {
   
	    if(is_array($value)) {
	        // recursive call //
	       arrayToXml($value, $xml,'ITEM');
	   } else {
	       // set tags in uppercase if needed //
	        if ($upper == true) {
	             $key = strtoupper($key);
	           }
	           // append to XML string //
	           $xml .= "<$key>" . htmlspecialchars(trim($value)) . "</$key>";
	   }
	}
	// close tag if needed //
	if ($wrap != null) {
	   $xml .= "</$wrap>\n";
	}
}
/**
 * Function returns HTML string for input associative array.
 * @param Array $array Input associative array
 * @param String $tag Wrapping tag
 * Note: Function is an adaptation from -- http://www.redips.net/php/convert-array-to-xml/ 
 */
function arrayToHTML($array, &$html, $tag) {
	// wrap html with $tag //
	if ($tag != null) {
	    $html .= "<HTML>\n";
	}
	// main loop //
	foreach ($array as $key=>$value) {
    
	   if(is_array($value)) {
        // recursive call //
	       arrayToHTML($value, $html,'h1');
	    } else {
        // set tags in uppercase if needed //
	      if ($upper == true) {
	           $key = strtoupper($key);
	       }
        // append to XML string //
	       $html .= "<$key>" . strtoupper($key) . ' : ' . htmlspecialchars(trim($value)) . "</$key><br>";
	   }
	}
	// close tag if needed //
	if ($tag != null) {
	   $html .= "</HTML>\n";
	}	
}	

/**
 * Returns all existing Catalog's Publishers = equipment's manufactures
 * @return Array of equipment's manufactures currently available on Catalogos.
**/
function getAllPublishers() {
	 
	 global $dbhost, $dbuser, $dbpwd, $dbname, $dbport;
	// Create connection
	 $mysqli = new mysqli($dbhost, $dbuser, $dbpwd, $dbname, $dbport); 
	 $publishers = array(); 
	  /* check connection */
	 if (mysqli_connect_errno()) {   
		  error_log("Connect failed: " . mysqli_connect_error());  
		  echo '{"success":0,"error_message":"' . mysqli_connect_error() . '"}';
	 } else {   	
		  if ($stmt = $mysqli->prepare("SELECT DISTINCT catalogosPublisher FROM catalogos")){
		  /* execute query */ 
		    $stmt->execute();
		  /* bind result variables */
		    $stmt->bind_result($name);  // +
		  
		  /* fetch values */
		    while($stmt->fetch()){
		     $tmp = array();
		     //	$tmp["id"] = $id;
		     $tmp["name"] = $name;
		     array_push($publishers, $tmp);
		    }							
		    /* close statement */
		    $stmt->close();
		  }
	 }
  /* close connection */
	$mysqli->close();	
				 
	return $publishers;
}
/**
 * Returns all Lines from a Catalog's Publisher.
 * @param $pub_name The Publisher's name for the desired Lines.
 * @return Array of Lines for this Publisher. (with catalogos_Id) **
 * An empty array is returned if no lines from this publisher is found.
 **/
function publisherLines($pub_name){
	 
	 global $dbhost, $dbuser, $dbpwd, $dbname, $dbport;
	 // Create connection
	 $mysqli = new mysqli($dbhost, $dbuser, $dbpwd, $dbname, $dbport);
	 $lines = array();
	 /* check connection */
	 if (mysqli_connect_errno()) {
		  error_log("Connect failed: " . mysqli_connect_error());
		  echo '{"success":0,"error_message":"' . mysqli_connect_error() . '"}';
	 } else { 
		  if ($stmt = $mysqli->prepare("SELECT catalogosName FROM catalogos WHERE catalogosPublisher=?")){
			   /* bind parameters for markers */
			   $stmt->bind_param("s", $pub_name);
			   /* execute query */
			   $stmt->execute();
			   /* bind result variables */
			   $stmt->bind_result($name);  // + $id,  catalogos_Id, 
			   /* fetch values */
			   while ($stmt->fetch()) { // while($row = mysql_fetch_array( $stmt ))
				 $lines[] = $name;    
			   }
			   /* close statement */
			   $stmt->close();
		  } 
	 }	  
	/* close connection */
	$mysqli->close();											  
	return $lines;
}
/**
 * Function returns one product by giving it's reference number.
 * @param $refnum The Product's Reference Number
 * @return Array  with 0 or 1 Product
 **/
function searchProduct($refNum){
//	 ("SELECT * FROM catalogos.catalogosConteudo WHERE productReferenceNum =?")
    	 global $dbhost, $dbuser, $dbpwd, $dbname, $dbport;
	 $mysqli = new mysqli($dbhost, $dbuser, $dbpwd, $dbname, $dbport);
	 $products = array();
  /* check connection */
	 if (mysqli_connect_errno()) {
				error_log("Connect failed: " . mysqli_connect_error());
				echo '{"success":0,"error_message":"' . mysqli_connect_error() . '"}';
				// , b.length
	 } else {
		  $sql = "SELECT b.catalogosEntryName, b.productReferenceNum, b.photoFileName,
		  b.manufacturer, b.productDescription, b.weight, b.hight, b.length, b.width FROM catalogosConteudo b WHERE b.productReferenceNum =? ";
		  if ($stmt = $mysqli->prepare($sql)) {
		   /* bind parameters for markers */
			   $stmt->bind_param("s", $refNum);
			   /* execute query */
			   $stmt->execute();
			   /* bind result variables */
			   $stmt->bind_result($name, $refnum, $path, $brand, $description, $weight, $height, $length, $width);  // 
			    /* fetch values */
			   while ($stmt->fetch()) {
			       $tmp = array();    
			       $tmp['RefNum'] = $refnum; 
			       $tmp['Name'] = $name;   
			       $tmp['Image'] = $path;
			       $tmp['Maker'] = $brand;
			       $tmp['Description'] = $description;
			       $tmp['Weight'] = $weight;
			       $tmp['Hight'] = $height;
			       $tmp['Length'] = $length;
			       $tmp['Width'] = $width;
			       array_push($products, $tmp);
			   }
			   /* close statement */
			   $stmt->close();	  	   
		  }	   
	 }/* close connection */
	$mysqli->close();	  
	return $products;	 	 
}

/**
 * Function returns existing categories for a Line of Publisher.
 * @param $pub_name The Publisher's name
 * @param $line_name The Line for the desired Categories 
 * @return Array of Categories or Category for the given Line from the Publisher.
 **/
function categoriesFromLine($pub_name,$line_name){
	 
	global $dbhost, $dbuser, $dbpwd, $dbname, $dbport;
	$mysqli = new mysqli($dbhost, $dbuser, $dbpwd, $dbname, $dbport);
	 $categories = array();
  /* check connection */
	 if (mysqli_connect_errno()) {
		  error_log("Connect failed: " . mysqli_connect_error());
		  echo '{"success":0,"error_message":"' . mysqli_connect_error() . '"}';
	 } else {
		  $sql = "SELECT catalogosLevelName FROM catalogosEstrutura e INNER JOIN catalogos c ON e.catalogos_Id = c.catalogos_Id WHERE e.parentCatalogosLevelNum IS NULL AND c.catalogosPublisher=? AND c.catalogosName =?";
		  if ($stmt = $mysqli->prepare($sql)) {   
		   /* bind parameters for markers */
		    $stmt->bind_param("ss", $pub_name, $line_name);
		   /* execute query */
		    $stmt->execute();
		   /* bind result variables */
		    $stmt->bind_result($name);
		   /* fetch values */
		    while ($stmt->fetch()) { 
		         $categories[] = $name;    
		    }
		  /* close statement */
		    $stmt->close();
		  }
	 } /* close connection */
	$mysqli->close();
	return $categories;
}
// function linesForCategory($pub_name, $cat_name){}

/**
 * Returns all sub-Categories from a Category's Line from one Publisher
 * @param $pub_name The Publisher's name
 * @param $line_name The Line 
 * @param $cat_name The Category 
 * @return Array of sub-categories 
 **/ 
 function subCategoriesFromLine($pub_name,$line_name,$cat_name){
	 
	 global $dbhost, $dbuser, $dbpwd, $dbname, $dbport;
	 $mysqli = new mysqli($dbhost, $dbuser, $dbpwd, $dbname, $dbport);
	 $subCategories = array();
  /* check connection */
	 if (mysqli_connect_errno()) {
				error_log("Connect failed: " . mysqli_connect_error());
				echo '{"success":0,"error_message":"' . mysqli_connect_error() . '"}';
	 } else {
		  $sql = "SELECT e.catalogosLevelName FROM catalogosEstrutura e INNER JOIN catalogos c ON e.catalogos_Id = c.catalogos_Id INNER JOIN catalogosEstrutura d ON e.parentCatalogosLevelNum = d.catalogosLevelNum WHERE d.catalogosLevelName=? AND c.catalogosPublisher=? AND c.catalogosName =?";
		  if ($stmt = $mysqli->prepare($sql)) {
		   /* bind parameters for markers */
			   $stmt->bind_param("sss", $cat_name, $pub_name, $line_name);
			   /* execute query */
			   $stmt->execute();
			   /* bind result variables */
			   $stmt->bind_result($name);
			    /* fetch values */
			   while ($stmt->fetch()) { 
			       $subCategories[] = $name;    
			   }
			   /* close statement */
			   $stmt->close();	  	   
		  }	   
	 }/* close connection */
	$mysqli->close();	  
	return $subCategories;
}

/**
 * Returns all Products from a Sub-category in one (Category's) Line from a Publisher
 * @param $pub_name The Publisher's name
 * @param $line_name The Line 
 * @param $subcat_name  Sub-category = type of products
 * @return Array of products 
 **/ 
function productsFromType($pub_name,$line_name, $subcat_name){
	 
	 global $dbhost, $dbuser, $dbpwd, $dbname, $dbport;
	 $mysqli = new mysqli($dbhost, $dbuser, $dbpwd, $dbname, $dbport);
	 $products = array();
  /* check connection */
	 if (mysqli_connect_errno()) {
				error_log("Connect failed: " . mysqli_connect_error());
				echo '{"success":0,"error_message":"' . mysqli_connect_error() . '"}';
				// , b.length
	 } else {
		  $sql = "SELECT b.catalogosEntryName, b.productReferenceNum, b.photoFileName,
		  b.manufacturer, b.productDescription, b.weight, b.hight, b.length, b.width FROM catalogosConteudo b INNER JOIN catalogosEstrutura e ON b.catalogosEntryName = e.catalogosLevelName INNER JOIN catalogos c ON e.catalogos_Id = c.catalogos_Id INNER JOIN catalogosEstrutura d ON e.parentCatalogosLevelNum = d.catalogosLevelNum WHERE d.catalogosLevelName=? AND c.catalogosPublisher=? AND c.catalogosName =?";
		  if ($stmt = $mysqli->prepare($sql)) {
		   /* bind parameters for markers */
			   $stmt->bind_param("sss", $subcat_name, $pub_name, $line_name);
			   /* execute query */
			   $stmt->execute();
			   /* bind result variables */
			   $stmt->bind_result($name, $refnum, $path, $brand, $description, $weight, $height, $length, $width);  // 
			    /* fetch values */
			   while ($stmt->fetch()) {
			       $tmp = array();    
			       $tmp['RefNum'] = $refnum; 
			       $tmp['Name'] = $name;   
			       $tmp['Image'] = $path;
			       $tmp['Maker'] = $brand;
			       $tmp['Description'] = $description;
			       $tmp['Weight'] = $weight;
			       $tmp['Hight'] = $height;
			       $tmp['Length'] = $length;
			       $tmp['Width'] = $width;
			       array_push($products, $tmp);
			   }
			   /* close statement */
			   $stmt->close();	  	   
		  }	   
	 }/* close connection */
	$mysqli->close();	  
	return $products;
}
// http://stackoverflow.com/questions/10195451/sql-inner-join-with-3-tables

/* --- Step 3: Process Request **/
 
    $method = $_GET['method'];          
 	// build payload //
    $response['code'] = 1;
    $response['api_version'] = '1.0.0';
    $response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
	
 switch($method) {
        case 'Publishers':
		  $response['data'] = getAllPublishers();				
		  break;
        case 'Lines': 
            if ($_GET['pub_name']) {
		 //  echo('Lines!!...');
		  $response['data'] = publisherLines( $_GET['pub_name']);	  
	    }
            break;
	 case 'Search':
	     if ($_GET['refNum']) {
		  $response['data'] = searchProduct( $_GET['refNum']);	  
	    }
            break; 
        case 'Categories':
            if ( $_GET['pub_name']  && $_GET['line_name'] ){
		  $response['data'] = categoriesFromLine($_GET['pub_name'], $_GET['line_name']);
	    }	
            break;
        case 'subCategories':
	    if ( $_GET['pub_name']  && $_GET['line_name']  && $_GET['cat_name']){
		  $response['data'] = subCategoriesFromLine($_GET['pub_name'], $_GET['line_name'], $_GET['cat_name']);
	    }
            break;
	 case 'Products':
	    if ( $_GET['pub_name']  && $_GET['line_name']  && $_GET['subcat_name']){
		 // echo('Products pls');
		  $response['data'] = productsFromType($_GET['pub_name'], $_GET['line_name'], $_GET['subcat_name']);
	    }
            break;
        default:
          //  $this->_response('Invalid Method', 405); $response['status'] = 
            break;
        }

// http://www.gym2gym.com.br/v2/catalogsAPI.php?method=Publishers&format=json
// http://www.gym2gym.com.br/v2/catalogsAPI.php?method=Lines&pub_name=TECHNOGYM&format=json
// https://www.gym2gym.com.br/v2/catalogsAPI.php?method=Categories&pub_name=TECHNOGYM&line_name=ARTIS&format=json
// https://www.gym2gym.com.br/v2/catalogsAPI.php?method=subCategories&pub_name=TECHNOGYM&line_name=ARTIS&cat_name=CARDIO&format=json

// http://www.gym2gym.com.br/v2/catalogsAPI.php?method=Products&pub_name=TECHNOGYM&line_name=ARTIS&subcat_name=Upper Body&format=json
// http://www.gym2gym.com.br/v2/catalogsAPI.php?method=Search&refNum=SS-ADC&format=json

// Method A: Say Hello to the API - http://www.gym2gym.com.br/v2/catalogsAPI.php?method=hello&format=json
// https://equipo-powerfit.rhcloud.com/v2/catalogsAPI.php?method=hello&format=json
if( strcasecmp($_GET['method'],'hello') == 0){
	$response['code'] = 1;
	$response['status'] = $api_response_code[ $response['code'] ]['HTTP Response'];
 	$response['data'] = 'Hello World';
 //   $row = $result->fetch_row();
   // echo $row[0];
  //  $response['data'] = $row[0]; 
}

// --- Step 4: Deliver Response

// Return Response to browser
   deliver_response($_GET['format'], $response);

?>